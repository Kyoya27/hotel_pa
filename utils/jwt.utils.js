var jwt = require('jsonwebtoken');
var jwtUtils = {};

const JWT_SIGN_SECRET = 'fhdufhzihfuiehf54f6ed4f4dfzs4vdfvfv46848vfv4f';

jwtUtils.generateToken = function(user) {
  return jwt.sign({
    id: user.id,
    isAdmin: user.admin
  }, JWT_SIGN_SECRET, {
    expiresIn: '10h'
  });
}

jwtUtils.checkToken = function(request, response, next) {
  jwt.verify(request.headers.authorization, JWT_SIGN_SECRET, function(err, decoded) {
    if(err) {
      return response.json({ 'error': 'Failed to authenticate token' });
    } else {
      request.decoded = decoded;
			request.id_user = decoded.id;
      next();
    }
  })
}

jwtUtils.checkTokenAdmin = function(request, response, next) {
	var auth = request.headers.authorization;
	if(auth) {
		jwt.verify(auth, JWT_SIGN_SECRET, function(err, decoded) {
			if(err) {
				return response.json({ 'error': 'Failed to authenticate token' });
			} else {
				if(decoded.isAdmin === 1){
					request.decoded = decoded;
					request.id_user = decoded.id;
					next();
				} else {
					return response.json({ 'error': 'User not admin' });
				}
			}
		});
	} else {
		return response.status(500).end({ "error": "No token sent" });
	}
}

module.exports = jwtUtils;