const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const EventController = controllers.EventController;

const eventRouter = express.Router();
eventRouter.use(bodyParser.json());

//id, name, beginning, description
eventRouter.post('/add', jwt.checkToken, function(req, res) {
  const name = req.body.name;
  const beginning = req.body.beginning;
  const description = req.body.description;
  /*const id_user = req.body.id_user;*/
  
  if(name === undefined || beginning === undefined || description === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  EventController.createEvent(name, beginning, description/*, id_user*/)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the event" });
  });
});

eventRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  EventController.findAll(req.query.id, req.query.name, req.query.beginning, req.query.description,  req.query.available, limit, offset)
  .then((event) => {
    res.json(event);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*eventRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  EventController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

eventRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const name = req.body.name;
  const beginning = req.body.beginning;
  const description = req.body.description;
  const available = req.body.available;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  EventController.update(id, name, beginning, description, available)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = eventRouter;
