const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const TableBookingController = controllers.TableBookingController;

const tableBookingRouter = express.Router();
tableBookingRouter.use(bodyParser.json());

//id, arrival, total_price, done, id_hotel_booking
tableBookingRouter.post('/add', function(req, res) {
  const arrival = req.body.arrival;
  const done = req.body.done;
  const id_table = req.body.id_table;
  const id_hotel_booking = req.body.id_hotel_booking;
  
  if(arrival === undefined || done === undefined || id_table === undefined || id_hotel_booking === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  TableBookingController.createTableBooking(arrival, done, id_table, id_hotel_booking)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the tableBooking" });
  });
});

tableBookingRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  TableBookingController.findAll(req.query.id, req.query.arrival, req.query.total_price, req.query.done, req.query.id_table, req.query.id_hotel_booking, limit, offset)
  .then((tableBooking) => {
    res.json(tableBooking);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*tableBookingRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  TableBookingController.update(id, undefined, false)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

tableBookingRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const arrival = req.body.arrival;
  const total_price = req.body.total_price;
  const done = req.body.done;
  const id_table = req.body.id_table;
  const id_hotel_booking = req.body.id_hotel_booking;
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  TableBookingController.update(id, arrival, total_price, done, id_table, id_hotel_booking)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = tableBookingRouter;
