const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const MenuController = controllers.MenuController;

const menuRouter = express.Router();
menuRouter.use(bodyParser.json());

//id, name, available
menuRouter.post('/add', jwt.checkToken, function(req, res) {
  const name = req.body.name;
  const available = req.body.available;
  
  if(name === undefined || available === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  MenuController.createMenu(name, available)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the menu" });
  });
});

menuRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  MenuController.findAll(req.query.id, req.query.name, req.query.available, limit, offset)
  .then((menu) => {
    res.json(menu);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

menuRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  MenuController.update(id, undefined, false)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});

menuRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const name = req.body.name;
  const available = req.body.available;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  MenuController.update(id, name, available)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = menuRouter;
