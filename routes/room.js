const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const RoomController = controllers.RoomController;

const roomRouter = express.Router();
roomRouter.use(bodyParser.json());

//id, cleared, id_room_status
roomRouter.post('/add', jwt.checkToken, function(req, res) {
  const number = req.body.number;
  const description = req.body.description;
  const seats = req.body.seats;
  const cleared = req.body.cleared;
  const id_room_status = req.body.id_room_status;
  var price = 52.5;
  for(var i = 1; i < seats; i++){
	  price += (1.5*i)*52.5
  }
  
  if(number === undefined || description === undefined || seats === undefined || cleared === undefined || id_room_status === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  RoomController.createRoom(number, description, seats, cleared, price, id_room_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the room" });
  });
});

roomRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  RoomController.findAll(req.query.id, req.query.number, req.query.description, req.query.seats, req.query.cleared, req.query.price, req.query.available, req.query.id_room_status, limit, offset)
  .then((room) => {
    res.json(room);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*roomRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  RoomController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

roomRouter.post('/update'/*, jwt.checkToken*/, function(req, res) {
  const id = req.body.id;
  const number = req.body.number;
  const description = req.body.description;
  const seats = req.body.seats;
  const cleared = req.body.cleared;
  const price = req.body.price;
  const available = req.body.available;
  const id_room_status = req.body.id_room_status;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  RoomController.update(id, number, description, seats,  cleared, price, available, id_room_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = roomRouter;
