const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const DishTypeController = controllers.DishTypeController;

const dishTypeRouter = express.Router();
dishTypeRouter.use(bodyParser.json());

//id, name
dishTypeRouter.post('/add', jwt.checkToken, function(req, res) {
  const name = req.body.name;
  
  if(name === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  DishTypeController.createDishType(name)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the dishType" });
  });
});

dishTypeRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  DishTypeController.findAll(req.query.id, req.query.name, limit, offset)
  .then((dishType) => {
    res.json(dishType);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*dishTypeRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  DishTypeController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

dishTypeRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const name = req.body.name;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  DishTypeController.update(id, name)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = dishTypeRouter;
