const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const DishController = controllers.DishController;

const dishRouter = express.Router();
dishRouter.use(bodyParser.json());

//id, name, price, available, id_dish_type, id_menu
dishRouter.post('/add', jwt.checkToken, function(req, res) {
  const name = req.body.name;
  const price = req.body.price;
  const available = req.body.available;
  const id_dish_type = req.body.id_dish_type;
  const id_menu = req.body.id_menu;
  
  if(name === undefined || price === undefined || available === undefined || id_dish_type === undefined ) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  DishController.createDish(name, price, available, id_dish_type, id_menu)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the dish" });
  });
});

dishRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  DishController.findAll(req.query.id, req.query.name, req.query.price, req.query.available, req.query.id_dish_type, req.query.id_menu, limit, offset)
  .then((dish) => {
    res.json(dish);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

dishRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  DishController.update(id, undefined, undefined, false, undefined, undefined)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});

dishRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const name = req.body.name;
  const available = req.body.available;
  const price = req.body.price;
  const id_dish_type = req.body.id_dish_type;
  const id_menu = req.body.id_menu;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  DishController.update(id, name, price, available, id_dish_type, id_menu)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = dishRouter;
