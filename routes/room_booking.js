const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const RoomBookingController = controllers.RoomBookingController;

const roomBookingRouter = express.Router();
roomBookingRouter.use(bodyParser.json());
//id, arrival, departure, booked_at, adult_number, child_number, id_room, id_booking, id_room_booking_status
roomBookingRouter.post('/add', function(req, res) {
  const arrival = req.body.arrival;
  const departure = req.body.departure;
  const adult_number = req.body.adult_number;
  const id_room = req.body.id_room;
  const id_booking = req.body.id_booking;
  const id_room_booking_status = req.body.id_room_booking_status;
  
  if(arrival === undefined || departure === undefined || adult_number === undefined || id_room === undefined || id_booking === undefined || id_room_booking_status === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  RoomBookingController.createRoomBooking(arrival, departure, adult_number, id_room, id_booking, id_room_booking_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the roomBooking" });
  });
});

roomBookingRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  RoomBookingController.findAll(req.query.id, req.query.arrival, req.query.departure, req.query.booked_at, req.query.adult_number, req.query.id_room, req.query.id_booking, req.query.id_room_booking_status, limit, offset)
  .then((roomBooking) => {
    res.json(roomBooking);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*roomBookingRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  RoomBookingController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

roomBookingRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const arrival = req.body.arrival;
  const departure = req.body.departure;
  const booked_at = req.body.booked_at;
  const adult_number = req.body.adult_number;
  const id_room = req.body.id_room;
  const id_booking = req.body.id_booking;
  const id_room_booking_status = req.body.id_room_booking_status;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  RoomBookingController.update(id, arrival, departure, booked_at, adult_number, id_room, id_booking, id_room_booking_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = roomBookingRouter;
