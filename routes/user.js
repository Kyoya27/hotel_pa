const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const UserController = controllers.UserController;

const userRouter = express.Router();
userRouter.use(bodyParser.json());

userRouter.get('/checkMail', function(req, res) {
  const email = req.query.email ? req.query.email : undefined;
  
  if(email === null) {
    res.status(400).json({ 'error': 'Invalid parameters' });
  }
  
  UserController.checkUserEmail(email)
  .then((user) => {
    res.json(user);
  })
  .catch((err) => {
		res.status(404).json({ 'error': 'Invalid identifiers' });
  });
});

userRouter.post('/login', function(req, res) {
  const email = req.body.email;
  const password = req.body.password;
  
  if(email === null || password === null) {
    res.status(400).json({ 'error': 'Invalid parameters' });
  }
  
  UserController.checkUserEmail(email)
  .then((user) => {
    if(user) {
      bcrypt.compare(password, user.password, function(err, result) {
        if(result) {
			
          if(user.enabled === 1) {
            res.status(200).json({
              'id': user.id,
              'lastname': user.lastname,
              'firstname': user.firstname,
              'email': user.email,
              'password': user.password,
              'date_insc': user.date_insc,
              'birthday': user.birthday,
              'admin': user.admin,
              'active': user.active,
              'enabled': user.enabled,
              'token': jwt.generateToken(user)
            });
          } else {
            res.status(403).json({ "error": "Account disabled" })
          }
        } else {
          res.status(404).json({ 'error': 'Invalid identifiers' });
        }
      });
    } else {
			res.status(404).json({ 'error': 'Invalid identifiers' });
    }
  })
  .catch((err) => {
		res.status(404).json({ 'error': 'Invalid identifiers' });
  });
});

userRouter.get('/', jwt.checkToken, function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  UserController.findAll(req.query.id, req.query.firstname, req.query.lastname, req.query.birthday,  req.query.email,  req.query.date_insc, req.query.admin, req.query.active, req.query.enabled, limit, offset)
  .then((user) => {
    res.json(user);
  })
  .catch((err) => {
    res.status(500).end();
  });
});

userRouter.post('/update', jwt.checkToken, function(req, res) {
	
  const id = req.body.id;
  const email = req.body.email;
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const birthday = req.body.birthday;
  const admin = req.body.admin;
  const active = req.body.active;
  const enabled = req.body.enabled;
  if(req.body.id === undefined) {
    res.status(400).end();
    return;
  }
  

	UserController.update(req.body.id, req.body.email, req.body.firstname, req.body.lastname, req.body.birthday, req.body.admin, req.body.active, req.body.enabled)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

userRouter.post('/register', function(req, res) {
console.log(req.body);
  const email = req.body.email;
  const password1 = req.body.password;
  const password2 = req.body.password2;
  const lastname = req.body.lastname;
  const firstname = req.body.firstname;
  const birthday = req.body.birthday;
  var admin;
  if(req.body.admin !== undefined){
	  admin = req.body.admin
  }
  else{
	  admin = 0
  }

  if(email === undefined || password1 === undefined || password2 === undefined || lastname === undefined || firstname === undefined || birthday === undefined){
    res.status(400).json({ 'error': 'parametres invalides' });
  }
  
  if(password1 !== password2) {
    res.status(500).json({ 'error': 'passwords are different' });
  } else {
    UserController.checkUserEmail(email)
    .then((user) => {
      if(!user) {
					bcrypt.hash(password1, 5, function(err, bcryptpwd) {

						UserController.createUser(email, bcryptpwd, lastname, firstname, new Date(birthday).toISOString(), admin)
							.then((newUser) => {
							res.status(201).json({ 'id': newUser.id });
						})
						.catch((err) => {
              console.log("2");
							res.status(500).json({ 'error': 'User creation failed' });
						});
				})
      } else {
              console.log("4");
        res.status(409).json({ 'error': 'Email already exists' });
      }
    })
		.catch((err) => {
              console.log("5");
			res.status(500).json({ 'error': 'User creation failed' });
		});
  }
});

module.exports = userRouter;
