const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const ServiceBookingController = controllers.ServiceBookingController;

const serviceBookingRouter = express.Router();
serviceBookingRouter.use(bodyParser.json());
//id, booked_at, id_service, id_booking, id_service_booking_status
serviceBookingRouter.post('/add', jwt.checkToken, function(req, res) {
  const id_service = req.body.id_service;
  const id_booking = req.body.id_booking;
  const id_service_booking_status = req.body.id_service_booking_status;
  
  if(id_service === undefined || id_booking === undefined || id_service_booking_status === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  ServiceBookingController.createServiceBooking(id_service, id_booking, id_service_booking_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the serviceBooking" });
  });
});

serviceBookingRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  ServiceBookingController.findAll(req.query.id, req.query.booked_at, req.query.realised_at, req.query.available, req.query.id_service, req.query.id_booking, req.query.id_service_booking_status, limit, offset)
  .then((serviceBooking) => {
    res.json(serviceBooking);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*serviceBookingRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  ServiceBookingController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

serviceBookingRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const booked_at = req.body.booked_at;
  const realised_at = req.body.realised_at;
  const available = req.body.available;
  const id_service = req.body.id_service;
  const id_booking = req.body.id_booking;
  const id_service_booking_status = req.body.id_service_booking_status;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  ServiceBookingController.update(id, booked_at, realised_at, available, id_service, id_booking, id_service_booking_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = serviceBookingRouter;
