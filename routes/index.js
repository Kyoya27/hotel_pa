const RouteManager = function() { };

RouteManager.attach = function(app) {
  app.use('/user', require('./user')),
  app.use('/booking', require('./booking'))
  app.use('/booking_status', require('./booking_status'))
  app.use('/event', require('./event'))
  app.use('/event_booking', require('./event_booking'))
  app.use('/event_booking_status', require('./event_booking_status'))
  app.use('/room', require('./room'))
  app.use('/room_booking', require('./room_booking'))
  app.use('/room_booking_status', require('./room_booking_status'))
  app.use('/room_status', require('./room_status'))
  app.use('/service', require('./service'))
  app.use('/service_booking', require('./service_booking'))
  app.use('/service_booking_status', require('./service_booking_status'))
  app.use('/dish', require('./dish'))
  app.use('/dish_type', require('./dish_type'))
  app.use('/dish_booking', require('./dish_booking'))
  app.use('/menu', require('./menu'))
  app.use('/table', require('./table'))
  app.use('/table_booking', require('./table_booking'))
  app.use('/dashboard', require('./dashboard'))
};

module.exports = RouteManager;