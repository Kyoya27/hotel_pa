const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const ServiceController = controllers.ServiceController;

const serviceRouter = express.Router();
serviceRouter.use(bodyParser.json());

//id, name, created_at,	realised_at, description, price
serviceRouter.post('/add', jwt.checkToken, function(req, res) {
  const name = req.body.name;
  const description = req.body.description;
  const price = req.body.price;
  
  if(name === undefined ||  description === undefined || price === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  ServiceController.createService(name, description, price)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the service" });
  });
});

serviceRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  ServiceController.findAll(req.query.id, req.query.name, req.query.created_at, req.query.realised_at, req.query.description,req.query.price,req.query.available,limit, offset)
  .then((service) => {
    res.json(service);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*serviceRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  ServiceController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

serviceRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const name = req.body.name;
  const created_at = req.body.created_at;
  const description = req.body.description;
  const price = req.body.price;
  const available = req.body.available;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  ServiceController.update(id, name, created_at, description, price, available)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = serviceRouter;
