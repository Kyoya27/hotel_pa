const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const TableController = controllers.TableController;

const tableRouter = express.Router();
tableRouter.use(bodyParser.json());

//id, x_pos, y_pos
tableRouter.post('/add', jwt.checkToken, function(req, res) {
  const x_pos = req.body.x_pos;
  const y_pos = req.body.y_pos;
  
  if(x_pos === undefined || y_pos === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  TableController.createTable(x_pos, y_pos)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the table" });
  });
});

tableRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  TableController.findAll(req.query.id, req.query.x_pos, req.query.y_pos, req.query.available, limit, offset)
  .then((table) => {
    res.json(table);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*tableRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  TableController.update(id, undefined, false)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

tableRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const x_pos = req.body.x_pos;
  const y_pos = req.body.y_pos;
  const available = req.body.available;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  TableController.update(id, x_pos, y_pos, available)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = tableRouter;
