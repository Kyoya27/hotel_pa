const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const EventBookingController = controllers.EventBookingController;

const eventBookingRouter = express.Router();
eventBookingRouter.use(bodyParser.json());
//id, booked_at, seats_number, id_event, id_booking, id_event_booking_status
eventBookingRouter.post('/add', jwt.checkToken, function(req, res) {
  const seats_number = req.body.seats_number;
  const id_event = req.body.id_event;
  const id_booking = req.body.id_booking;
  const id_event_booking_status = req.body.id_event_booking_status;
  
  if(id_event === undefined || id_booking === undefined || id_event_booking_status === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  EventBookingController.createEventBooking(seats_number, id_event, id_booking, id_event_booking_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the eventBooking" });
  });
});

eventBookingRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  EventBookingController.findAll(req.query.id, req.query.booked_at, req.query.seats_number, req.query.available, req.query.id_event, req.query.id_booking, req.query.id_event_booking_status, limit, offset)
  .then((eventBooking) => {
    res.json(eventBooking);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*eventBookingRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  EventBookingController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

eventBookingRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const booked_at = req.body.booked_at;
  const seats_number = req.body.seats_number;
  const available = req.body.available;
  const id_event = req.body.id_event;
  const id_booking = req.body.id_booking;
  const id_event_booking_status = req.body.id_event_booking_status;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  EventBookingController.update(id, booked_at, seats_number, available, id_event, id_booking, id_event_booking_status)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = eventBookingRouter;
