const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const DishBookingController = controllers.DishBookingController;

const dishBookingRouter = express.Router();
dishBookingRouter.use(bodyParser.json());

//id, id_table_booking, id_dish, id_menu
dishBookingRouter.post('/add', jwt.checkToken, function(req, res) {
  const id_table_booking = req.body.id_table_booking;
  const id_dish = req.body.id_dish;
  
  if(id_table_booking === undefined || id_dish === undefined ) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  DishBookingController.createDishBooking(id_table_booking, id_dish)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the dishBooking" });
  });
});

dishBookingRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  DishBookingController.findAll(req.query.id, req.query.id_table_booking, req.query.id_dish, limit, offset)
  .then((dishBooking) => {
    res.json(dishBooking);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*dishBookingRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  DishBookingController.update(id, undefined, undefined, 0, undefined, undefined)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

dishBookingRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const id_table_booking = req.body.id_table_booking;
  const id_dish = req.body.id_dish;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  DishBookingController.update(id, id_table_booking, id_dish)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = dishBookingRouter;
