const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const BookingController = controllers.BookingController;

const bookingRouter = express.Router();
bookingRouter.use(bodyParser.json());

//id, date_booking, start_date, end_date, staying_days, active
bookingRouter.post('/add', function(req, res) {
  const start_date = req.body.start_date;
  const end_date = req.body.end_date;
  const staying_days = req.body.staying_days;
  const id_booking_status = req.body.id_booking_status;
  const id_user = req.body.id_user;
  
  if(start_date === undefined || end_date === undefined || staying_days === undefined || id_booking_status === undefined || id_user === undefined) {
    res.status(400).json({ 'error': 'Invalid parameters' });
    return;
  }
  
  BookingController.createBooking(start_date, end_date, staying_days, id_booking_status, id_user)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end({ "error": "Can't add the booking" });
  });
});

bookingRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  BookingController.findAll(req.query.id, req.query.date_booking, req.query.start_date, req.query.end_date, req.query.staying_days, req.query.active, req.query.comment, req.query.id_booking_status, req.query.id_user,limit, offset)
  .then((booking) => {
    res.json(booking);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

/*bookingRouter.post('/delete/:id', jwt.checkToken, function(req, res) {
  const id =  req.params.id;
  if(isNan(parseInt(id, 10))) {
    res.status(404).end();
    return;
  }
  
  BookingController.update(id, undefined, undefined, undefined, 0)
  .then((p) => {
    res.status(200).json(p);
  })
  .catch((err) => {
    console.log(err);
    res.status(500).end();
  });
});*/

bookingRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const date_booking = req.body.date_booking;
  const start_date = req.body.start_date;
  const end_date = req.body.end_date;
  const staying_days = req.body.staying_days;
  const active = req.body.active;
  const comment = req.body.comment;
  const id_booking_status = req.body.id_booking_status;
  const id_user = req.body.id_user;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  BookingController.update(id, date_booking, start_date, end_date, staying_days, active, comment, id_booking_status, id_user)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = bookingRouter;
