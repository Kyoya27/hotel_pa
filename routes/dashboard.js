const express = require('express');
const bodyParser = require('body-parser');
const controllers = require('../controllers');
const bcrypt = require('bcrypt');
const jwt = require('../utils/jwt.utils');

const DashboardController = controllers.DashboardController;

const dashboardRouter = express.Router();
dashboardRouter.use(bodyParser.json());

//id, name
dashboardRouter.get('/', function(req, res) {
  const limit = req.query.limit ? parseInt(req.query.limit) : undefined;
  const offset = req.query.offset ? parseInt(req.query.offset) : undefined;
  DashboardController.findAll(req.query.id, req.query.presentation, req.query.rooms, req.query.events, req.query.services, req.query.restaurant, limit, offset)
  .then((dashboard) => {
    res.json(dashboard);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});

dashboardRouter.post('/update', jwt.checkToken, function(req, res) {
  const id = req.body.id;
  const presentation = req.body.presentation;
  const rooms = req.body.rooms;
  const events = req.body.events;
  const services = req.body.services;
  const restaurant = req.body.restaurant;
  
  if(id === undefined) {
    res.status(400).end();
    return;
  }
  
  DashboardController.update(id, presentation, rooms, events, services, restaurant)
  .then((p) => {
    res.status(201).json(p);
  })
  .catch((err) => {
    console.error(err);
    res.status(500).end();
  });
});


module.exports = dashboardRouter;
