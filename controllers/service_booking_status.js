const ModelIndex = require('../models');
const ServiceBookingStatus = ModelIndex.ServiceBookingStatus;
const Op = ModelIndex.Sequelize.Op;

const ServiceBookingStatusController = function() { };

//id, name

ServiceBookingStatusController.checkServiceBookingStatus = function(id) {
    return ModelIndex.ServiceBookingStatus.find({where: {id: id}});
}

ServiceBookingStatusController.createServiceBookingStatus = function(name) {
  return ServiceBookingStatus.create({
    name: name
  });
}

ServiceBookingStatusController.findAll = function(id, name, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return ServiceBookingStatus.findAll(options);
};

ServiceBookingStatusController.update = function(id, name)  {
  return ServiceBookingStatus.find({where: {id: id}})
  .then((ServiceBookingStatus) => {
    if(ServiceBookingStatus) {
      if(name === undefined) name = ServiceBookingStatus.name;
      return ServiceBookingStatus.updateAttributes({
        name: name
      });
    }
  })
}


module.exports = ServiceBookingStatusController
