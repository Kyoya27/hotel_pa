const ModelIndex = require('../models');
const ServiceBooking = ModelIndex.ServiceBooking;
const Op = ModelIndex.Sequelize.Op;

const ServiceBookingController = function() { };


ServiceBookingController.checkServiceBooking = function(id) {
    return ModelIndex.ServiceBooking.find({where: {id: id}});
}

ServiceBookingController.createServiceBooking = function(/*booked_at,*/ id_service, id_booking, id_service_booking_status) {
  return ServiceBooking.create({
    //booked_at: booked_at,
	realised_at: null,
    id_service: id_service,
    id_booking: id_booking,
    id_service_booking_status: id_service_booking_status
  });
}

ServiceBookingController.findAll = function(id, booked_at, realised_at, available, id_service, id_booking, id_service_booking_status, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(booked_at !== undefined){
    where.booked_at = {
      [Op.like]: `${booked_at}%`
    }
  }
  if(realised_at !== undefined){
    where.realised_at = {
      [Op.like]: `${realised_at}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }
  if(id_service !== undefined){
    where.id_service = {
      [Op.like]: `${id_service}%`
    }
  }
  if(id_booking !== undefined){
    where.id_booking = {
      [Op.like]: `${id_booking}%`
    }
  }
  if(id_service_booking_status !== undefined){
    where.id_service_booking_status = {
      [Op.like]: `${id_service_booking_status}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return ServiceBooking.findAll(options);
};

ServiceBookingController.update = function(id, booked_at, realised_at, available, id_service, id_booking, id_service_booking_status)  {
  return ServiceBooking.find({where: {id: id}})
  .then((ServiceBooking) => {
    if(ServiceBooking) {
      if(booked_at === undefined) booked_at = ServiceBooking.booked_at;
      if(realised_at === undefined) realised_at = ServiceBooking.realised_at;
      if(available === undefined) available = ServiceBooking.available;
      if(id_service === undefined) id_service = ServiceBooking.id_service;
      if(id_booking === undefined) id_booking = ServiceBooking.id_booking;
      if(id_service_booking_status === undefined) id_service_booking_status = ServiceBooking.id_service_booking_status;
      return ServiceBooking.updateAttributes({
        booked_at: booked_at,
        realised_at: realised_at,
        available: available,
        id_service: id_service,
        id_booking: id_booking,
        id_service_booking_status: id_service_booking_status
      });
    }
  })
}



module.exports = ServiceBookingController
