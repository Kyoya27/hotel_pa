const ModelIndex = require('../models');
const BookingStatus = ModelIndex.BookingStatus;
const Op = ModelIndex.Sequelize.Op;

const BookingStatusController = function() { };

//id, name

BookingStatusController.checkBookingStatus = function(id) {
    return ModelIndex.BookingStatus.find({where: {id: id}});
}

BookingStatusController.createBookingStatus = function(name) {
  return BookingStatus.create({
    name: name
  });
}

BookingStatusController.findAll = function(id, name, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return BookingStatus.findAll(options);
};

BookingStatusController.update = function(id, name)  {
  return BookingStatus.find({where: {id: id}})
  .then((BookingStatus) => {
    if(BookingStatus) {
      if(name === undefined) name = BookingStatus.name;
      return BookingStatus.updateAttributes({
        name: name
      });
    }
  })
}


module.exports = BookingStatusController
