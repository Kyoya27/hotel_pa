const ModelIndex = require('../models');
const Room = ModelIndex.Room;
const Op = ModelIndex.Sequelize.Op;

const RoomController = function() { };

//id, cleared, id_room_status

RoomController.checkRoom = function(id) {
    return ModelIndex.Room.find({where: {id: id}});
}

RoomController.createRoom = function(number, description, seats, cleared, price, id_room_status) {
  return Room.create({
	  number: number,
	  description: description,
	  seats: seats,
    cleared: cleared,
    price: price,
    id_room_status: id_room_status
  });
}

RoomController.findAll = function(id, number, description, seats, cleared, price, available, id_room_status, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(number !== undefined){
    where.number = {
      [Op.like]: `${number}%`
    }
  }
  if(description !== undefined){
    where.description = {
      [Op.like]: `${description}%`
    }
  }
  if(seats !== undefined){
    where.seats = {
      [Op.like]: `${seats}%`
    }
  }
  if(cleared !== undefined){
    where.cleared = {
      [Op.like]: `${cleared}%`
    }
  }
  if(price !== undefined){
    where.price = {
      [Op.like]: `${price}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }
  if(id_room_status !== undefined){
    where.id_room_status = {
      [Op.like]: `${id_room_status}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Room.findAll(options);
};

RoomController.update = function(id, number, description, seats, cleared, price, available, id_room_status)  {
  return Room.find({where: {id: id}})
  .then((Room) => {
    if(Room) {
      if(number === undefined) number = Room.number;
      if(description === undefined) description = Room.description;
      if(seats === undefined) seats = Room.seats;
      if(cleared === undefined) cleared = Room.cleared;
      if(price === undefined) price = Room.price;
      if(available === undefined) available = Room.available;
      if(id_room_status === undefined) id_room_status = Room.id_room_status;
      return Room.updateAttributes({
        number: number,
        description: description,
		seats: seats,
        cleared: cleared,
        price: price,
        available: available,
        id_room_status: id_room_status
      });
    }
  })
}



module.exports = RoomController
