const ModelIndex = require('../models');
const RoomBookingStatus = ModelIndex.RoomBookingStatus;
const Op = ModelIndex.Sequelize.Op;

const RoomBookingStatusController = function() { };

//id, name

RoomBookingStatusController.checkRoomBookingStatus = function(id) {
    return ModelIndex.RoomBookingStatus.find({where: {id: id}});
}

RoomBookingStatusController.createRoomBookingStatus = function(name) {
  return RoomBookingStatus.create({
    name: name
  });
}

RoomBookingStatusController.findAll = function(id, name, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return RoomBookingStatus.findAll(options);
};

RoomBookingStatusController.update = function(id, name)  {
  return RoomBookingStatus.find({where: {id: id}})
  .then((RoomBookingStatus) => {
    if(RoomBookingStatus) {
      if(name === undefined) name = RoomBookingStatus.name;
      return RoomBookingStatus.updateAttributes({
        name: name
      });
    }
  })
}


module.exports = RoomBookingStatusController
