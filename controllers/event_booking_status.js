const ModelIndex = require('../models');
const EventBookingStatus = ModelIndex.EventBookingStatus;
const Op = ModelIndex.Sequelize.Op;

const EventBookingStatusController = function() { };

//id, name

EventBookingStatusController.checkEventBookingStatus = function(id) {
    return ModelIndex.EventBookingStatus.find({where: {id: id}});
}

EventBookingStatusController.createEventBookingStatus = function(name) {
  return EventBookingStatus.create({
    name: name
  });
}

EventBookingStatusController.findAll = function(id, name, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return EventBookingStatus.findAll(options);
};

EventBookingStatusController.update = function(id, name)  {
  return EventBookingStatus.find({where: {id: id}})
  .then((EventBookingStatus) => {
    if(EventBookingStatus) {
      if(name === undefined) name = EventBookingStatus.name;
      return EventBookingStatus.updateAttributes({
        name: name
      });
    }
  })
}


module.exports = EventBookingStatusController
