const ModelIndex = require('../models');
const Event = ModelIndex.Event;
const Op = ModelIndex.Sequelize.Op;

const EventController = function() { };

//id, name, beginning, description

EventController.checkEvent = function(id) {
    return ModelIndex.Event.find({where: {id: id}});
}

EventController.createEvent = function(name, beginning, description) {
  return Event.create({
    name: name,
    beginning: beginning,
    description: description
  });
}

EventController.findAll = function(id, name, beginning, description, available, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }
  if(beginning !== undefined){
    where.beginning = {
      [Op.like]: `${beginning}%`
    }
  }
  if(description !== undefined){
    where.description = {
      [Op.like]: `${description}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Event.findAll(options);
};

EventController.update = function(id, name, beginning, description, available)  {
  return Event.find({where: {id: id}})
  .then((Event) => {
    if(Event) {
      if(name === undefined) name = Event.name;
      if(beginning === undefined) beginning = Event.beginning;
      if(description === undefined) description = Event.description;
      if(available === undefined) available = Event.available;
      return Event.updateAttributes({
        name: name,
        beginning: beginning,
        description: description,
        available: available
      });
    }
  })
}



module.exports = EventController
