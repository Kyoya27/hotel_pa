const ModelIndex = require('../models');
const DishBooking = ModelIndex.DishBooking;
const Op = ModelIndex.Sequelize.Op;

const DishBookingController = function() { };

//id, id_table_booking, id_dish, id_menu

DishBookingController.checkDishBooking = function(id) {
    return ModelIndex.DishBooking.find({where: {id: id}});
}

DishBookingController.createDishBooking = function(id_table_booking, id_dish) {
  return DishBooking.create({
    id_table_booking: id_table_booking,
    id_dish: id_dish
  });
}

DishBookingController.findAll = function(id, id_table_booking, id_dish, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(id_table_booking !== undefined){
    where.id_table_booking = {
      [Op.like]: `${id_table_booking}%`
    }
  }
  if(id_dish !== undefined){
    where.id_dish = {
      [Op.like]: `${id_dish}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return DishBooking.findAll(options);
};

DishBookingController.update = function(id, id_table_booking, id_dish)  {
  return DishBooking.find({where: {id: id}})
  .then((DishBooking) => {
    if(DishBooking) {
      if(id_table_booking === undefined) id_table_booking = DishBooking.id_table_booking;
      if(id_dish === undefined) id_dish = DishBooking.id_dish;
      return DishBooking.updateAttributes({
        id_table_booking: id_table_booking,
        id_dish: id_dish
      });
    }
  })
}



module.exports = DishBookingController
