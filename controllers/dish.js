const ModelIndex = require('../models');
const Dish = ModelIndex.Dish;
const Op = ModelIndex.Sequelize.Op;

const DishController = function() { };

//id, name, price, available, id_dish_type, id_menu

DishController.checkDish = function(id) {
    return ModelIndex.Dish.find({where: {id: id}});
}

DishController.createDish = function(name, price, available, id_dish_type, id_menu = null) {
  return Dish.create({
    name: name,
    price: price,
    available: available,
    id_dish_type: id_dish_type,
    id_menu: id_menu
  });
}

DishController.findAll = function(id, name, price, available, id_dish_type, id_menu,  limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }
  if(price !== undefined){
    where.price = {
      [Op.like]: `${price}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }
  if(id_dish_type !== undefined){
    where.id_dish_type = {
      [Op.like]: `${id_dish_type}%`
    }
  }
  if(id_menu !== undefined){
    where.id_menu = {
      [Op.like]: `${id_menu}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Dish.findAll(options);
};

DishController.update = function(id, name, price, available, id_dish_type, id_menu)  {
  return Dish.find({where: {id: id}})
  .then((Dish) => {
    if(Dish) {
      if(name === undefined) name = Dish.name;
      if(price === undefined) price = Dish.price;
      if(available === undefined) available = Dish.available;
      if(id_dish_type === undefined) id_dish_type = Dish.id_dish_type;
      if(id_menu === undefined) id_menu = Dish.id_menu;
      return Dish.updateAttributes({
        name: name,
        price: price,
        available: available,
        id_dish_type: id_dish_type,
        id_menu: id_menu
      });
    }
  })
}



module.exports = DishController
