const ModelIndex = require('../models');
const Menu = ModelIndex.Menu;
const Op = ModelIndex.Sequelize.Op;

const MenuController = function() { };

//id, name, available

MenuController.checkMenu = function(id) {
    return ModelIndex.Menu.find({where: {id: id}});
}

MenuController.createMenu = function(name, available) {
  return Menu.create({
    name: name,
    available: available
  });
}

MenuController.findAll = function(id, name, available, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Menu.findAll(options);
};

MenuController.update = function(id, name, available)  {
  return Menu.find({where: {id: id}})
  .then((Menu) => {
    if(Menu) {
      if(name === undefined) name = Menu.name;
      if(available === undefined) available = Menu.available;
      return Menu.updateAttributes({
        name: name,
        available: available
      });
    }
  })
}


module.exports = MenuController
