const ModelIndex = require('../models');
const Dashboard = ModelIndex.Dashboard;
const Op = ModelIndex.Sequelize.Op;

const DashboardController = function() { };

//id, name

DashboardController.checkDashboard = function(id) {
    return ModelIndex.Dashboard.find({where: {id: id}});
}

DashboardController.createDashboard = function(presentation, rooms, events, services, restaurant) {
  return Dashboard.create({
    presentation: presentation,
    rooms: rooms,
    events: events,
    services: services,
    restaurant: restaurant
  });
}

DashboardController.findAll = function(id, presentation, rooms, events, services, restaurant, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(presentation !== undefined){
    where.presentation = {
      [Op.like]: `${presentation}%`
    }
  }
  if(rooms !== undefined){
    where.rooms = {
      [Op.like]: `${rooms}%`
    }
  }
  if(events !== undefined){
    where.events = {
      [Op.like]: `${events}%`
    }
  }
  if(services !== undefined){
    where.services = {
      [Op.like]: `${services}%`
    }
  }
  if(restaurant !== undefined){
    where.restaurant = {
      [Op.like]: `${restaurant}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Dashboard.findAll(options);
};

DashboardController.update = function(id, presentation, rooms, events, services, restaurant)  {
  return Dashboard.find({where: {id: id}})
  .then((Dashboard) => {
    if(Dashboard) {
      if(presentation === undefined) presentation = Dashboard.presentation;
      if(rooms === undefined) rooms = Dashboard.rooms;
      if(events === undefined) events = Dashboard.events;
      if(services === undefined) services = Dashboard.services;
      if(restaurant === undefined) restaurant = Dashboard.restaurant;
      return Dashboard.updateAttributes({
        presentation: presentation,
        rooms: rooms,
        events: events,
        services: services,
        restaurant: restaurant
      });
    }
  })
}


module.exports = DashboardController
