const ModelIndex = require('../models');
const Service = ModelIndex.Service;
const Op = ModelIndex.Sequelize.Op;

const ServiceController = function() { };

//id, name, created_at,	realised_at, description, price

ServiceController.checkService = function(id) {
    return ModelIndex.Service.find({where: {id: id}});
}

ServiceController.createService = function(name, description, price) {
  return Service.create({
    name: name,
    description: description,
    price: price
  });
}

ServiceController.findAll = function(id, name, created_at, realised_at, description, price, available, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }
  if(created_at !== undefined){
    where.created_at = {
      [Op.like]: `${created_at}%`
    }
  }
  if(description !== undefined){
    where.description = {
      [Op.like]: `${description}%`
    }
  }
  if(price !== undefined){
    where.price = {
      [Op.like]: `${price}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Service.findAll(options);
};

ServiceController.update = function(id, name, created_at, description, price, available)  {
  return Service.find({where: {id: id}})
  .then((Service) => {
    if(Service) {
      if(name === undefined) name = Service.name;
      if(created_at === undefined) created_at = Service.created_at;
      if(description === undefined) description = Service.description;
      if(price === undefined) price = Service.price;
      if(available === undefined) available = Service.available;
      return Service.updateAttributes({
        name: name,
        created_at: created_at,
        description: description,
        price: price,
        available: available
      });
    }
  })
}



module.exports = ServiceController
