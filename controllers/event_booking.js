const ModelIndex = require('../models');
const EventBooking = ModelIndex.EventBooking;
const Op = ModelIndex.Sequelize.Op;

const EventBookingController = function() { };

//id, booked_at, seats_number, id_event, id_booking, id_event_booking_status

EventBookingController.checkEventBooking = function(id) {
    return ModelIndex.EventBooking.find({where: {id: id}});
}

EventBookingController.createEventBooking = function(/*booked_at,*/ seats_number = 0, id_event, id_booking, id_event_booking_status) {
  return EventBooking.create({
    //booked_at: booked_at,
    seats_number: seats_number,
    id_event: id_event,
    id_booking: id_booking,
    id_event_booking_status: id_event_booking_status
  });
}

EventBookingController.findAll = function(id, booked_at, seats_number, available, id_event, id_booking, id_event_booking_status, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(booked_at !== undefined){
    where.booked_at = {
      [Op.like]: `${booked_at}%`
    }
  }
  if(seats_number !== undefined){
    where.seats_number = {
      [Op.like]: `${seats_number}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }
  if(id_event !== undefined){
    where.id_event = {
      [Op.like]: `${id_event}%`
    }
  }
  if(id_booking !== undefined){
    where.id_booking = {
      [Op.like]: `${id_booking}%`
    }
  }
  if(id_event_booking_status !== undefined){
    where.id_event_booking_status = {
      [Op.like]: `${id_event_booking_status}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return EventBooking.findAll(options);
};

EventBookingController.update = function(id, booked_at, seats_number, available, id_event, id_booking, id_event_booking_status)  {
  return EventBooking.find({where: {id: id}})
  .then((EventBooking) => {
    if(EventBooking) {
      if(booked_at === undefined) booked_at = EventBooking.booked_at;
      if(seats_number === undefined) seats_number = EventBooking.seats_number;
      if(available === undefined) available = ServiceBooking.available;
      if(id_event === undefined) id_event = EventBooking.id_event;
      if(id_booking === undefined) id_booking = EventBooking.id_booking;
      if(id_event_booking_status === undefined) id_event_booking_status = EventBooking.id_event_booking_status;
      return EventBooking.updateAttributes({
        booked_at: booked_at,
        seats_number: seats_number,
        available: available,
        id_event: id_event,
        id_booking: id_booking,
        id_event_booking_status: id_event_booking_status
      });
    }
  })
}



module.exports = EventBookingController
