const ModelIndex = require('../models');
const DishType = ModelIndex.DishType;
const Op = ModelIndex.Sequelize.Op;

const DishTypeController = function() { };

//id, name

DishTypeController.checkDishType = function(id) {
    return ModelIndex.DishType.find({where: {id: id}});
}

DishTypeController.createDishType = function(name) {
  return DishType.create({
    name: name
  });
}

DishTypeController.findAll = function(id, name, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return DishType.findAll(options);
};

DishTypeController.update = function(id, name)  {
  return DishType.find({where: {id: id}})
  .then((DishType) => {
    if(DishType) {
      if(name === undefined) name = DishType.name;
      return DishType.updateAttributes({
        name: name
      });
    }
  })
}


module.exports = DishTypeController
