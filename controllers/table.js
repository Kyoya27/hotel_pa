const ModelIndex = require('../models');
const Table = ModelIndex.Table;
const Op = ModelIndex.Sequelize.Op;

const TableController = function() { };

//id, x_pos, y_pos

TableController.checkTable = function(id) {
    return ModelIndex.Table.find({where: {id: id}});
}

TableController.createTable = function(x_pos, y_pos) {
  return Table.create({
    x_pos: x_pos,
    y_pos: y_pos
  });
}

TableController.findAll = function(id, x_pos, y_pos, available, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(x_pos !== undefined){
    where.x_pos = {
      [Op.like]: `${x_pos}%`
    }
  }
  if(y_pos !== undefined){
    where.y_pos = {
      [Op.like]: `${y_pos}%`
    }
  }
  if(available !== undefined){
    where.available = {
      [Op.like]: `${available}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Table.findAll(options);
};

TableController.update = function(id, x_pos, y_pos, available)  {
  return Table.find({where: {id: id}})
  .then((Table) => {
    if(Table) {
      if(x_pos === undefined) x_pos = Table.x_pos;
      if(y_pos === undefined) y_pos = Table.y_pos;
      if(available === undefined) available = Table.available;
      return Table.updateAttributes({
        x_pos: x_pos,
        y_pos: y_pos,
        available: available
      });
    }
  })
}


module.exports = TableController
