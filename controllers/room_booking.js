const ModelIndex = require('../models');
const RoomBooking = ModelIndex.RoomBooking;
const Op = ModelIndex.Sequelize.Op;

const RoomBookingController = function() { };

//id, arrival, departure, booked_at, adult_number, child_number, id_room, id_booking, id_room_booking_status

RoomBookingController.checkRoomBooking = function(id) {
    return ModelIndex.RoomBooking.find({where: {id: id}});
}

RoomBookingController.createRoomBooking = function(arrival, departure, /*booked_at,*/ adult_number, id_room, id_booking, id_room_booking_status) {
  return RoomBooking.create({
    arrival: arrival,
    departure: departure,
    //booked_at: booked_at,
    adult_number: adult_number,
    id_room: id_room,
    id_booking: id_booking,
    id_room_booking_status: id_room_booking_status
  });
}

RoomBookingController.findAll = function(id,  arrival, departure, booked_at, adult_number, id_room, id_booking, id_room_booking_status, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(arrival !== undefined){
    where.arrival = {
      [Op.like]: `${arrival}%`
    }
  }
  if(departure !== undefined){
    where.departure = {
      [Op.like]: `${departure}%`
    }
  }
  if(booked_at !== undefined){
    where.booked_at = {
      [Op.like]: `${booked_at}%`
    }
  }
  
  if(adult_number !== undefined){
    where.adult_number = {
      [Op.like]: `${adult_number}%`
    }
  }
  if(id_room !== undefined){
    where.id_room = {
      [Op.like]: `${id_room}%`
    }
  }
  if(id_booking !== undefined){
    where.id_booking = {
      [Op.like]: `${id_booking}%`
    }
  }
  if(id_room_booking_status !== undefined){
    where.id_room_booking_status = {
      [Op.like]: `${id_room_booking_status}%`
    }
  }
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return RoomBooking.findAll(options);
};

RoomBookingController.update = function(id,  departure, booked_at, adult_number, id_room, id_booking, id_room_booking_status)  {
  return RoomBooking.find({where: {id: id}})
  .then((RoomBooking) => {
    if(RoomBooking) {
      if(departure === undefined) departure = RoomBooking.departure;
      if(booked_at === undefined) booked_at = RoomBooking.booked_at;
      if(adult_number === undefined) adult_number = RoomBooking.adult_number;
      if(id_room === undefined) id_room = RoomBooking.id_room;
      if(id_booking === undefined) id_booking = RoomBooking.id_booking;
      if(id_room_booking_status === undefined) id_room_booking_status = RoomBooking.id_room_booking_status;
      return RoomBooking.updateAttributes({
        departure: departure,
        booked_at: booked_at,
        adult_number: adult_number,
        id_room: id_room,
        id_booking: id_booking,
        id_room_booking_status: id_room_booking_status
      });
    }
  })
}



module.exports = RoomBookingController
