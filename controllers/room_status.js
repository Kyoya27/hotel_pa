const ModelIndex = require('../models');
const RoomStatus = ModelIndex.RoomStatus;
const Op = ModelIndex.Sequelize.Op;

const RoomStatusController = function() { };

//id, name

RoomStatusController.checkRoomStatus = function(id) {
    return ModelIndex.RoomStatus.find({where: {id: id}});
}

RoomStatusController.createRoomStatus = function(name) {
  return RoomStatus.create({
    name: name
  });
}

RoomStatusController.findAll = function(id, name, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(name !== undefined){
    where.name = {
      [Op.like]: `${name}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return RoomStatus.findAll(options);
};

RoomStatusController.update = function(id, name)  {
  return RoomStatus.find({where: {id: id}})
  .then((RoomStatus) => {
    if(RoomStatus) {
      if(name === undefined) name = RoomStatus.name;
      return RoomStatus.updateAttributes({
        name: name
      });
    }
  })
}


module.exports = RoomStatusController
