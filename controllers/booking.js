const ModelIndex = require('../models');
const Booking = ModelIndex.Booking;
const Op = ModelIndex.Sequelize.Op;

const BookingController = function() { };

//id, date_booking, start_date, end_date, staying_days

BookingController.checkBooking = function(id) {
    return ModelIndex.Booking.find({where: {id: id}});
}

BookingController.createBooking = function(start_date, end_date, staying_days, id_booking_status, id_user) {
  return Booking.create({
    start_date: start_date,
    end_date: end_date,
	staying_days: staying_days,	
    id_booking_status: id_booking_status,
	id_user: id_user
	
  });
}

BookingController.findAll = function(id, date_booking, start_date, end_date, staying_days, active, comment, id_booking_status, id_user, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(date_booking !== undefined){
    where.date_booking = {
      [Op.like]: `${date_booking}%`
    }
  }
  if(start_date !== undefined){
    where.start_date = {
      [Op.like]: `${start_date}%`
    }
  }
  if(end_date !== undefined){
    where.end_date = {
      [Op.like]: `${end_date}%`
    }
  }
  if(staying_days !== undefined){
    where.staying_days = {
      [Op.like]: `${staying_days}%`
    }
  }
  if(active !== undefined){
    where.active = {
      [Op.like]: `${active}%`
    }
  }
  if(comment !== undefined){
    where.comment = {
      [Op.like]: `${comment}%`
    }
  }
  if(id_booking_status !== undefined){
    where.id_booking_status = {
      [Op.like]: `${id_booking_status}%`
    }
  }
  if(id_user !== undefined){
    where.id_user = {
      [Op.like]: `${id_user}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return Booking.findAll(options);
};

BookingController.update = function(id, date_booking, start_date, end_date, staying_days, active, comment, id_booking_status, id_user)  {
  return Booking.find({where: {id: id}})
  .then((Booking) => {
    if(Booking) {
      if(date_booking === undefined) date_booking = Booking.date_booking;
      if(start_date === undefined) start_date = Booking.start_date;
      if(end_date === undefined) end_date = Booking.end_date;
      if(staying_days === undefined) end_date = Booking.staying_days;
      if(active === undefined) active = Booking.active;
      if(comment === undefined) comment = Booking.comment;
      if(id_booking_status === undefined) id_booking_status = Booking.id_booking_status;
      if(id_user === undefined) id_user = Booking.id_user;
      return Booking.updateAttributes({
        date_booking: date_booking,
        start_date: start_date,
        end_date: end_date,
        staying_days: staying_days,
        active: active,	
        comment: comment,	
		id_booking_status: id_booking_status,
		id_user: id_user
      });
    }
  })
}

/*BookingController.findDetailedBooking = function(id) {
    return Booking.findAll({
        raw: true,
        include: [{
                model: ModelIndex.ServiceBooking,
                include: [{
                    model: ModelIndex.Service
                }]
            }, {
                model: ModelIndex.EventBooking,
                include: [{
                    model: ModelIndex.Event
                }]
            }, {
                model: ModelIndex.RestaurantBooking,
                include: [{
                    model: ModelIndex.Table
                }]
            }],
        where: { id: id }
    })
}*/


module.exports = BookingController
