const ModelIndex = require('../models');
const TableBooking = ModelIndex.TableBooking;
const Op = ModelIndex.Sequelize.Op;

const TableBookingController = function() { };

//id, arrival, total_price, done, id_hotel_booking

TableBookingController.checkTableBooking = function(id) {
    return ModelIndex.TableBooking.find({where: {id: id}});
}

TableBookingController.createTableBooking = function(arrival, done, id_table, id_hotel_booking) {
  return TableBooking.create({
    arrival: arrival,
    done: done,
    id_table: id_table,
    id_hotel_booking: id_hotel_booking
  });
}

TableBookingController.findAll = function(id, arrival, total_price, done, id_table, id_hotel_booking, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(arrival !== undefined){
    where.arrival = {
      [Op.like]: `${arrival}%`
    }
  }
  if(total_price !== undefined){
    where.total_price = {
      [Op.like]: `${total_price}%`
    }
  }
  if(done !== undefined){
    where.done = {
      [Op.like]: `${done}%`
    }
  }
  if(id_table !== undefined){
    where.id_table = {
      [Op.like]: `${id_table}%`
    }
  }
  if(id_hotel_booking !== undefined){
    where.id_hotel_booking = {
      [Op.like]: `${id_hotel_booking}%`
    }
  }

  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  
  return TableBooking.findAll(options);
};

TableBookingController.update = function(id, arrival, total_price, done, id_table, id_hotel_booking)  {
  return TableBooking.find({where: {id: id}})
  .then((TableBooking) => {
    if(TableBooking) {
      if(arrival === undefined) arrival = TableBooking.arrival;
      if(total_price === undefined) total_price = TableBooking.total_price;
      if(done === undefined) done = TableBooking.done;
      if(id_table === undefined) id_table = TableBooking.id_table;
      if(id_hotel_booking === undefined) id_hotel_booking = TableBooking.id_hotel_booking;
      return TableBooking.updateAttributes({
        arrival: arrival,
        total_price: total_price,
        done: done,
        id_table: id_table,
        id_hotel_booking: id_hotel_booking
      });
    }
  })
}


module.exports = TableBookingController
