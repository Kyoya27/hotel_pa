const ModelIndex = require('../models');
const User = ModelIndex.User;
const Op = ModelIndex.Sequelize.Op;

const UserController = function() { };

UserController.checkUserEmail = function(email) {
  return User.findOne({
    where: {
      email: email
    }
  })
}

UserController.createUser = function(email, password, lastname, firstname, birthday, admin) {
  return User.create({
    email: email,
    password: password,
    lastname: lastname,
    firstname: firstname,
    birthday: birthday, 
	admin : admin
  });
}

UserController.findAll = function(id, firstname, lastname, birthday, email, date_insc, admin, active, enabled, limit, offset) {
  const where = {};
  const options = {};
  
  if(id !== undefined){
    where.id = {
      [Op.like]: `${id}%`
    }
  }
  if(lastname !== undefined){
    where.lastname = {
      [Op.like]: `${lastname}%`
    }
  }
  if(firstname !== undefined){
    where.firstname = {
      [Op.like]: `${firstname}%`
    }
  }
  if(birthday !== undefined){
    where.birthday = {
      [Op.like]: `${birthday}%`
    }
  }
  if(email !== undefined){
    where.email = {
      [Op.like]: `${email}%`
    }
  }
  if(date_insc !== undefined){
    where.date_insc = {
      [Op.like]: `${date_insc}%`
    }
  }
  if(admin !== undefined){
    where.admin = {
      [Op.like]: `${admin}%`
    }
  }
  if(active !== undefined){
    where.active = {
      [Op.like]: `${active}%`
    }
  }
  if(enabled !== undefined){
    where.enabled = {
      [Op.like]: `${enabled}%`
    }
  }

  
  options.where = where;
  if(limit !== undefined){
    options.limit = limit;
  }
  if(offset !== undefined){
    options.offset = offset;
  }
  
  return User.findAll(options);
};

UserController.update = function(id, email, firstname, lastname, birthday, admin, active, enabled) {
  return User.find({where: {id: id}})
  .then((User) => {
    if(User) {
      if(email === undefined) email = User.email;
      if(lastname === undefined) lastname = User.lastname;
      if(firstname === undefined) firstname = User.firstname;
      if(birthday === undefined) birthday = User.birthday;
      if(admin === undefined) admin = User.admin;
      if(active === undefined) active = User.active;
      if(enabled === undefined) enabled = User.enabled;
      return User.updateAttributes({
        email: email,
        lastname: lastname,
        firstname: firstname,
        birthday: birthday,
        admin: admin,
        active: active,
        enabled: enabled
      });
    }
  })
}

module.exports = UserController
