module.exports = function (sequelize, DataTypes) {
	const Dashboard = sequelize.define('Dashboard', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	presentation: {
      type: DataTypes.STRING,
      allowNull: false
    },
	rooms: {
      type: DataTypes.STRING,
      allowNull: false
    },
	events: {
      type: DataTypes.STRING,
      allowNull: false
    },
	services: {
      type: DataTypes.STRING,
      allowNull: false
    },
	restaurant: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	Dashboard.associate = _associate;
	return Dashboard;
}

// INTERNAL
function _associate(models) {
	
}