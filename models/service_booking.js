module.exports = function (sequelize, DataTypes) {
	const ServiceBooking = sequelize.define('ServiceBooking', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	booked_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
	realised_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
	id_service: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_booking: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_service_booking_status: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	ServiceBooking.associate = _associate;
	return ServiceBooking;
}

// INTERNAL
function _associate(models) {
	models.ServiceBooking.belongsTo(models.Service, { foreignKey: 'id_service', targetKey: 'id' });
	models.ServiceBooking.belongsTo(models.Booking, { foreignKey: 'id_booking', targetKey: 'id' });
	models.ServiceBooking.belongsTo(models.ServiceBookingStatus, { foreignKey: 'id_service_booking_status', targetKey: 'id' });
}