module.exports = function (sequelize, DataTypes) {
	const EventBooking = sequelize.define('EventBooking', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	booked_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    seats_number: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
	id_event: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_booking: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_event_booking_status: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	EventBooking.associate = _associate;
	return EventBooking;
}

// INTERNAL
function _associate(models) {
	models.EventBooking.belongsTo(models.Event, { foreignKey: 'id_event', targetKey: 'id' });
	models.EventBooking.belongsTo(models.Booking, { foreignKey: 'id_booking', targetKey: 'id' });
	models.EventBooking.belongsTo(models.EventBookingStatus, { foreignKey: 'id_event_booking_status', targetKey: 'id' });
}