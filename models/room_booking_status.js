module.exports = function (sequelize, DataTypes) {
	const RoomBookingStatus = sequelize.define('RoomBookingStatus', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	RoomBookingStatus.associate = _associate;
	return RoomBookingStatus;
}

// INTERNAL
function _associate(models) {
	models.RoomBooking.hasMany(models.RoomBooking, { foreignKey: 'id_room_booking_status', sourceKey: 'id' });
}