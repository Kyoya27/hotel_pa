module.exports = function (sequelize, DataTypes) {
	const BookingStatus = sequelize.define('BookingStatus', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	BookingStatus.associate = _associate;
	return BookingStatus;
}

// INTERNAL
function _associate(models) {
	models.BookingStatus.hasMany(models.Booking, { foreignKey: 'id_booking_status', sourceKey: 'id' });
}