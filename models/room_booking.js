module.exports = function (sequelize, DataTypes) {
	const RoomBooking = sequelize.define('RoomBooking', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	arrival: {
      type: DataTypes.DATE,
      allowNull: false
    },
	departure: {
      type: DataTypes.DATE,
      allowNull: false
    },
	booked_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    adult_number: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
	id_room: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_booking: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_room_booking_status: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	RoomBooking.associate = _associate;
	return RoomBooking;
}

// INTERNAL
function _associate(models) {
	models.RoomBooking.belongsTo(models.Room, { foreignKey: 'id_room', targetKey: 'id' });
	models.RoomBooking.belongsTo(models.Booking, { foreignKey: 'id_booking', targetKey: 'id' });
	models.RoomBooking.belongsTo(models.RoomBookingStatus, { foreignKey: 'id_room_booking_status', targetKey: 'id' });
}