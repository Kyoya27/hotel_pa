module.exports = function (sequelize, DataTypes) {
	const ServiceBookingStatus = sequelize.define('ServiceBookingStatus', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	ServiceBookingStatus.associate = _associate;
	return ServiceBookingStatus;
}

// INTERNAL
function _associate(models) {
	models.ServiceBooking.hasMany(models.ServiceBooking, { foreignKey: 'id_service_booking_status', sourceKey: 'id' });
}