module.exports = function (sequelize, DataTypes) {
	const Service = sequelize.define('Service', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
	price: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
    //id_payedstatus
    //id_bookingstatus
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	Service.associate = _associate;
	return Service;
}

// INTERNAL
function _associate(models) {
	models.Service.hasMany(models.ServiceBooking, { foreignKey: 'id_service', sourceKey: 'id' });

}