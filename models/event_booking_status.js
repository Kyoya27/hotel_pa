module.exports = function (sequelize, DataTypes) {
	const EventBookingStatus = sequelize.define('EventBookingStatus', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	EventBookingStatus.associate = _associate;
	return EventBookingStatus;
}

// INTERNAL
function _associate(models) {
	models.EventBooking.hasMany(models.EventBooking, { foreignKey: 'id_event_booking_status', sourceKey: 'id' });
}