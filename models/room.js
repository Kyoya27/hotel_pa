module.exports = function (sequelize, DataTypes) {
	const Room = sequelize.define('Room', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
    number: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    seats: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 1
    },
    cleared: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 52.5
    },
    available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
	id_room_status: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	Room.associate = _associate;
	return Room;
}

// INTERNAL
function _associate(models) {
	models.Room.belongsTo(models.RoomStatus, { foreignKey: 'id_room_status', targetKey: 'id' });
}