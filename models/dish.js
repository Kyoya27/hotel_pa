module.exports = function (sequelize, DataTypes) {
	const Dish = sequelize.define('Dish', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    },
	price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
	available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
	id_dish_type: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_menu: {
		type: DataTypes.BIGINT,
		allowNull: true,
		foreignKey: true
	}
	}, {
		paranoid: false,
		underscored: true,
		freezeDishName: true,
		timestamps: false
	});
	Dish.associate = _associate;
	return Dish;
}

// INTERNAL
function _associate(models) {
	models.Dish.belongsTo(models.DishType, { foreignKey: 'id_dish_type', targetKey: 'id' });
	models.Dish.belongsTo(models.Menu, { foreignKey: 'id_menu', targetKey: 'id' });
}