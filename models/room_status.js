module.exports = function (sequelize, DataTypes) {
	const RoomStatus = sequelize.define('RoomStatus', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	RoomStatus.associate = _associate;
	return RoomStatus;
}

// INTERNAL
function _associate(models) {
	models.Room.hasMany(models.Room, { foreignKey: 'id_room_status', sourceKey: 'id' });
}