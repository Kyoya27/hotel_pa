module.exports = function (sequelize, DataTypes) {
	const Menu = sequelize.define('Menu', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
		name: {
      type: DataTypes.STRING,
      allowNull: false
    },
		available: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	return Menu;
}
