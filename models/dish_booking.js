module.exports = function(sequelize, DataTypes){
	const DishBooking = sequelize.define("DishBooking",{
		id:{
			type: DataTypes.BIGINT,
			primaryKey: true,
			autoIncrement: true
		},
		id_table_booking: {
			type: DataTypes.BIGINT,
			allowNull: false,
			foreignKey: true
		},
		id_dish: {
			type: DataTypes.BIGINT,
			allowNull: false,
			foreignKey: true
		}
	}, {
		underscored: true,
		timestamps: false,
		freezeTableName: true
	});
	DishBooking.associate = _associate;
	return DishBooking;
}

function _associate(models){
	models.DishBooking.belongsTo(models.Dish, { foreignKey: 'id_dish', targetKey: 'id' });
	models.DishBooking.belongsTo(models.TableBooking, { foreignKey: 'id_table_booking', targetKey: 'id' });
}