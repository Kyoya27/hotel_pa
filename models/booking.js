module.exports = function (sequelize, DataTypes) {
	const Booking = sequelize.define('Booking', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	date_booking: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
    start_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    end_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    staying_days: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    active: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 1
    },
    comment: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: ""
    },
	id_booking_status: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_user: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    }
    //id_payedstatus
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	Booking.associate = _associate;
	return Booking;
}

// INTERNAL
function _associate(models) {
	models.Booking.hasMany(models.ServiceBooking, { foreignKey: 'id_booking', sourceKey: 'id' });
	models.Booking.hasMany(models.RoomBooking, { foreignKey: 'id_booking', sourceKey: 'id' });
	models.Booking.hasMany(models.EventBooking, { foreignKey: 'id_booking', sourceKey: 'id' });
	models.Booking.hasMany(models.TableBooking, { foreignKey: 'id_hotel_booking', sourceKey: 'id' });
	models.Booking.belongsTo(models.User, {foreignKey: 'id_user', targetKey: 'id' });
	models.Booking.belongsTo(models.BookingStatus, {foreignKey: 'id_booking_status', targetKey: 'id' });
}