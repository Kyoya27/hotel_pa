module.exports = function (sequelize, DataTypes) {
	const Event = sequelize.define('Event', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    beginning: {
      type: DataTypes.DATE,
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false
    },
    available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
    //id_payedstatus
    //id_bookingstatus
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	Event.associate = _associate;
	return Event;
}

// INTERNAL
function _associate(models) {
	models.Event.hasMany(models.EventBooking, { foreignKey: 'id_booking', sourceKey: 'id' });

}