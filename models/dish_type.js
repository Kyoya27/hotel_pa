module.exports = function (sequelize, DataTypes) {
	const DishType = sequelize.define('DishType', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
      type: DataTypes.STRING,
      allowNull: false
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	DishType.associate = _associate;
	return DishType;
}

// INTERNAL
function _associate(models) {
	models.Dish.hasMany(models.Dish, { foreignKey: 'id_dish_type', sourceKey: 'id' });
}