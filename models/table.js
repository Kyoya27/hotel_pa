module.exports = function (sequelize, DataTypes) {
	const Table = sequelize.define('Table', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
    x_pos: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
	y_pos: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
    available: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	Table.associate = _associate;
	return Table;
}

// INTERNAL
function _associate(models) {
	
}