module.exports = function (sequelize, DataTypes) {
	const TableBooking = sequelize.define('TableBooking', {
	id: {
		type: DataTypes.BIGINT,
		primaryKey: true,
		autoIncrement: true
	},
	arrival: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW
    },
	total_price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
	done: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
	id_table: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
	id_hotel_booking: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    }
	}, {
		paranoid: false,
		underscored: true,
		freezeTableName: true,
		timestamps: false
	});
	TableBooking.associate = _associate;
	return TableBooking;
}

// INTERNAL
function _associate(models) {
	models.TableBooking.belongsTo(models.Table, { foreignKey: 'id_table', targetKey: 'id' });
	models.TableBooking.belongsTo(models.Booking, { foreignKey: 'id_hotel_booking', targetKey: 'id' });
}